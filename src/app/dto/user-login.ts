export class userLogin {
    email: string;
    password: string;
}

export class userCad {
    name: String;
    email: String;
    cpfOuCnpj: String;
    type: String;
    password: String;
    _id? : Number;
}