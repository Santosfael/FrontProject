export interface Order {
    moment: Date;
    orderStatus: String;
    client_id: Number;
    _id ?: Number;
}