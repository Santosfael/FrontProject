export interface OrderItem {
    quantity: Number;
    price: Number;
    productName: String;
    productImgUrl: String;
    _id ?: Number;
}