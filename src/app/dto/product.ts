export interface Product {
    name: String;
    description: String;
    price: number;
    imgUrl: String;
    _id ?: Number;
}