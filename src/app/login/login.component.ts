import { Component, OnInit, EventEmitter } from '@angular/core';
import { AuthService } from '../guards/auth.service';
import { userLogin, userCad } from '../dto/user-login';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  name: string;
  cpfOuCnpj: string;
  email: string;
  password: string;
  type: string;

  typePerson: string[] = ['Pessoa Física','Pessoa Jurídica'];

  private userAuth: boolean = false;

  private userLogin: userLogin = new userLogin();
  private userCad: userCad = new userCad();

  constructor(private _authService: AuthService,
              private _router: Router,
              private serviceLogin: LoginService,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  login() {
    this._authService.login(this.userLogin)
      .subscribe(res =>{
        //this.userLogin = res
        localStorage.setItem('token', res.token);
        this._router.navigate(['/home']);
        this.userAuth = true;

      },
      err => {
        this.userLogin = err;
        this.userAuth = false;
      });
  }

  userAuthenticated() {
    return this.userAuth;
  }

  insertUser() {
    if(this.userCad.type == 'Pessoa Física') {
      this.userCad.type = 'PESSOAFISICA';
    } else if(this.userCad.type == 'Pessoa Jurídica') {
      this.userCad.type = 'PESSOAJURIDICA';
    }
    this.serviceLogin.cadastro(this.userCad).subscribe(
      res => {
        this.notify('Usuário cadastrado');
        this._router.navigate(['/']);
      }
    )
  }

  notify(msg: string) {
    this.snackBar.open(msg, "OK", {duration: 3000});
  }

}
