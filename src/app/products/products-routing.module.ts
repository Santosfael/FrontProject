import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductFormComponent } from './product-form/product-form.component';
import { ProductsComponent } from './products.component';
import { AuthGuard } from '../guards/auth.guard';
import { ProductGuard } from '../guards/produts.guard';



const productRoutes: Routes = [
    {
    path: 'products',
    component: ProductsComponent,
    canActivate: [AuthGuard],
    canActivateChild: [ProductGuard],
    },
  {
    path: 'products/new',
    component: ProductFormComponent,
    canActivate: [AuthGuard],
    canActivateChild: [ProductGuard],
  },
  {
    path: 'products/edit/:id',
    component: ProductFormComponent,
    
  }
];

@NgModule({
  imports: [RouterModule.forChild(productRoutes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
