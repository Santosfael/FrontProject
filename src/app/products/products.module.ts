import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {ProductsComponent} from './products.component'
import { ProductFormComponent } from './product-form/product-form.component';
import { ProductListComponent } from './product-list/product-list.component'; 
import { ProductGuard } from '../guards/produts.guard';
import { ProductService } from '../services/product.service';
import { ProductsRoutingModule } from './products-routing.module';
import { MaterialModule } from '../material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material';
import { DialogProductComponent } from './dialog-product/dialog-product.component';



@NgModule({
  declarations: [
    ProductsComponent,
    ProductFormComponent,
    ProductListComponent,
    DialogProductComponent,
  ],
  entryComponents: [DialogProductComponent],
  imports: [
    MaterialModule,
    CommonModule,
    ProductsRoutingModule,
    ReactiveFormsModule,
    MatDialogModule,
    FormsModule,
  ],
  providers: [ProductGuard, ProductService]
})
export class ProductsModule { }
