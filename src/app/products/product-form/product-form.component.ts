import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductService } from 'src/app/services/product.service';
import { MatSnackBar } from '@angular/material';
import { Router} from '@angular/router';
import { Product } from 'src/app/dto/product';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  prodName: String = '';
  prodDescription: String = '';
  prodPrice: number;
  prodImgUrl: String = '';

  editProd: Product = null;

  productForm: FormGroup =  this.formProduct.group({
    _id: [null],
    name: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(20)]],
    description: ['',[Validators.required, Validators.minLength(20), Validators.maxLength(254)]],
    price: 0,
    imgUrl: '',
  });

  constructor(private formProduct: FormBuilder,
     private serviceProduct: ProductService,
     private snackBar: MatSnackBar,
     private _router: Router) { }

  ngOnInit() {
    
  }

  saveProduct() {
     if (this.productForm.valid) {
      this.serviceProduct.add({name: this.prodName,
          description: this.prodDescription,
          price: this.prodPrice,
          imgUrl: this.prodImgUrl})
        .subscribe((prod) => {
          this.notify('Produto salvo');
          this._router.navigate(['/products']);
        })
      //console.log('produto salvo');
     }
  }
  
  notify(msg: string) {
    this.snackBar.open(msg, "OK", {duration: 3000});
  }

}
