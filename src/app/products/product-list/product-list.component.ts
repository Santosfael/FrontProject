import { Component, OnInit, Inject } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/app/dto/product';
import { takeUntil } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { MatSnackBar, MatDialog } from '@angular/material';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { DialogProductComponent } from '../dialog-product/dialog-product.component';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  prodName: String = '';
  prodDescription: String = '';
  prodPrice: number;
  prodImgUrl: String = '';
  products: Product[];

  prodEdit: Product = null;
  
  products$: Observable<Product[]>;

  private unsubscribe$: Subject<Product[]> = new Subject();
  
  constructor(private productService: ProductService,
              private snackBar: MatSnackBar,
              private _router: Router,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.findAll();
    this.onRefresh();
  }

  onRefresh() {
    this.findAll();
  }

  findAll() {
    this.productService.get()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe((prods) => {
      this.products = prods['content'];
    }, (error) => {
      if(error instanceof HttpErrorResponse) {
        if(error.status === 401) {
          this._router.navigate(['/login']);
        }
      }
    });
  }

  editProduct(prod: Product) {
    this.prodName = Object.values(prod)[1];
    this.prodDescription = Object.values(prod)[2];
    this.prodPrice = Object.values(prod)[3];
    this.prodImgUrl = '';
    this.prodEdit = prod;

    const dialogRef = this.dialog.open(DialogProductComponent, {
      data: {name: this.prodName,
          description: this.prodDescription,
          price: this.prodPrice,
          imgUrl: this.prodImgUrl}
    });
    
    dialogRef.afterClosed().subscribe(result => {
      this.productService.update({name: result.name,
            description: result.description,
            price: result.price,
            imgUrl: '',
            _id: Object.values(prod)[0]}).subscribe(
        (success) => {
          
          this.notify('Produto alterado com sucesso'),
          this.onRefresh()
        },
        (err) => console.log(err)
      )});
  }

  deleteProduct(prod: Product) {
   let id = Object.values(prod)[0];
    console.log(id)
    this.productService.remove(id)
      .subscribe(
        (success) => {
          this.notify('Produto removido'),
          this.onRefresh()
        },
        (err) => console.log(err)
      );
  }

  notify(msg: string) {
    this.snackBar.open(msg, "OK", {duration: 3000});
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
  }
}