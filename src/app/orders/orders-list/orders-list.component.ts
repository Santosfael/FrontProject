import { Component, OnInit, ViewChild } from '@angular/core';
import { OrderService } from 'src/app/services/order.service';
import { Order } from 'src/app/dto/order';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { OrderItem } from 'src/app/dto/orderItens';

@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.css']
})
export class OrdersListComponent implements OnInit {

  orders: Order[];
  orderItens: OrderItem[];

  private unsubscribe$: Subject<Order[]> = new Subject();

  constructor(private serviceOrder: OrderService,
              private _router: Router) { }

  ngOnInit() {
    this.findAll();
  }

  findAll() {
    this.serviceOrder.getOrder()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe((ord) => {
      this.orders = ord;
    }, (error) => {
      if(error instanceof HttpErrorResponse) {
        if(error.status === 401) {
          //this._router.navigate(['/login']);
        }
      }
    });
  }
  detalheCompra(id) {
    this.serviceOrder.getOrderItem(id)
      .subscribe(orderList => {
        this.orderItens = orderList[''];
        console.log(this.orderItens);
      });
  }
}
