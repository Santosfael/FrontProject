import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrdersComponent } from './orders.component';
import { AuthGuard } from '../guards/auth.guard';
import { OrderGuard } from '../guards/order.guard';



const ordersRoutes: Routes = [
    {
      path: 'orders',
      component: OrdersComponent,
      canActivate: [AuthGuard],
      canActivateChild: [OrderGuard]
    },    
];

@NgModule({
  imports: [RouterModule.forChild(ordersRoutes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
