import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Product } from '../dto/product';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { take, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  headers = new HttpHeaders();

  readonly url: string;

  private productSubject$: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>(null);
  private loaded: boolean = false;

  constructor(private http: HttpClient) {
    this.url = 'http://localhost:8080/products';
    
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    })
  };

  get(): Observable<Product[]> {
    if(!this.loaded) {
      return this.http.get<Product[]>(this.url);
    }
    
  }

  add(product: Product): Observable<Product> {
    return this.http.post<Product>(this.url, product, this.httpOptions);
  }

  remove(id) {
    return this.http.delete(`${this.url}/${id}`, this.httpOptions);
  }

  loadById(id) {
    return this.http.get<Product>(`${this.url}/${id}`, this.httpOptions).pipe(take(1));
  }

  update(prod: Product){
    return this.http.put(`${this.url}/${prod._id}`, prod, this.httpOptions).pipe(take(1));
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };
}
