import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { userCad } from '../dto/user-login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  readonly urlCad = 'http://localhost:8080/users';
  constructor(private http: HttpClient,) { }

  cadastro(user) {
    return this.http.post(this.urlCad, user);
  }
}
