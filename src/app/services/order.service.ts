import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Order } from '../dto/order';
import { OrderItem } from '../dto/orderItens';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  hearder = new HttpHeaders;
  
  readonly urlOrder = 'http://localhost:8080/orders/myorders';
  readonly urlOrderItem = 'http://localhost:8080/orders';

  private loaded: boolean = false;

  constructor(private http: HttpClient) { }
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    })
  };

  getOrder(): Observable<Order[]> {
    if(!this.loaded) {
      return this.http.get<Order[]>(this.urlOrder, this.httpOptions);
      this.loaded = true;
    }
  }
  getOrderItem(id): Observable<OrderItem> {
    if(!this.loaded) {
      return this.http.get<OrderItem>(`${this.getOrderItem}/${1}/items`, this.httpOptions);
    }
  }
}
