import { Component } from '@angular/core';
import { AuthService } from '../guards/auth.service';

@Component({
  selector: 'app-nav-app-system',
  templateUrl: './nav-app-system.component.html',
  styleUrls: ['./nav-app-system.component.css']
})
export class NavAppSystemComponent {

  constructor(private authService: AuthService) {}

  ngOnInit() {

  }
}
